package ua.org.oa.il_il.pz3;

/**
 * 3. Задана строка произвольного размера. Создайте из этой строки новую, в которой 2 последних символа исходной строки перенесены в начало.
 */
public class String3 {
    private String string;
    public void string3(){
        System.out.println("Исходное слово : " + string);
        StringBuilder sb = new StringBuilder(string);
        sb.insert(0,string.substring(string.length()-2,string.length()));
        System.out.println("Результат :" + sb);
    }

    public String3(String string) {
        this.string = string;
    }
}
