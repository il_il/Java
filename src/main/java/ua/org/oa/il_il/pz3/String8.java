package ua.org.oa.il_il.pz3;

/**
 * 8. Заданы две строки. Создайте новую строку, состоящую из первой строки,
 * в которой удалены все вхождения второй строки.
 */
public class String8 {
    private String string;
    private String unstring;
    public void String8(){
        System.out.println("Строка : " + string);
        System.out.println("Под строка : " + unstring);
        System.out.println("Результат : " + string.replaceAll(unstring, ""));

    }

    public String8(String string, String string1) {
        this.string = string;
        this.unstring = string1;
    }
    
}
