package ua.org.oa.il_il.pz2;


public class SearchWord {

    private String word;
    private String text;

    public SearchWord(String word, String text) {
        this.word = word;
        this.text = text;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int words() { // Разбиение на слова текста
        int count = 0;
        String[] newText = text.toLowerCase().split("[,;:.!?\\s]+");
        for (String s:newText) {
            if(s == word){
                count ++;
            }
        }
        return count;
    }
}
