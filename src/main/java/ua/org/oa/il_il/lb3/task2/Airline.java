package ua.org.oa.il_il.lb3.task2;

import java.util.ArrayList;

public class Airline {
    private ArrayList<Planes> airPark = new ArrayList<>();

    public void aboveTheAverageForThisAirline(){ //- выше среднего по данной авиакомпании;
        int sum = 0;
        for (Planes s: airPark ) {
            System.out.println(s);
           sum += s.getCarryingCapacity();
        }
        for (Planes s:airPark
             ) {
            if(s.getCarryingCapacity()>sum){
                System.out.println(s.toString());
            }
        }
    }
    public void aboveACertainValue(int value){ //- выше определенного значения;

        for (Planes s:airPark) {
            if (s.getCarryingCapacity() > value) {
                System.out.println(s.toString());
            }
        }
    }

    public void belowAverageForThisAirline(){ //- ниже среднего по данной авиакомпании;
        int sum = 0;
        for (Planes s: airPark ) {
            System.out.println(s);
            sum += s.getCarryingCapacity();
        }
        for (Planes s:airPark
                ) {
            if(s.getCarryingCapacity()<sum){
                System.out.println(s.toString());
            }
        }
    }

    public void belowACertainValue(int value){ //- below определенного значения;

        for (Planes s:airPark) {
            if (s.getCarryingCapacity() < value) {
                System.out.println(s.toString());
            }
        }
    }
    public Airline(ArrayList<Planes> airPark) {
        this.airPark = airPark;
    }
    public ArrayList<Planes> getAirPark() {
        return airPark;
    }
    public void setAirPark(ArrayList<Planes> airPark) {
        this.airPark = airPark;
    }
    public void setAirPark(Planes plane){
        airPark.add(plane);
    }

    @Override
    public String toString() {
        return "Airline{" +
                "airPark=" + airPark +
                '}';
    }
}
