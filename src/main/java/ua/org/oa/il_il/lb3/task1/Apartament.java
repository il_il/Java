package ua.org.oa.il_il.lb3.task1;


public class Apartament {
    private int rooms;
    private int pepople;
    private int floor;
    private float area;
    private String city;

    public Apartament(){}; // пустой конструктор
    public Apartament(int rooms,int pepople,int floor,float area,String city){ // конструктор принимающий параметры
        this.rooms = rooms;
        this.pepople = pepople;
        this.floor = floor;
        this.area = area;
        this.city = city;
    }
    public Apartament(Apartament apartament){ // конструктор принимающий объект
        area = apartament.getArea();
        rooms = apartament.getRooms();
        pepople = apartament.getPepople();
        floor = apartament.getFloor();
        city = apartament.getCity();
    }

    public int getRooms() { //гетер для поля  rooms
        return rooms;
    }

    public void setRooms(int rooms) { //сетер для поля  rooms
        this.rooms = rooms;
    }

    public int getPepople() { // //гетер для поля  people
        return pepople;
    }

    public void setPepople(int pepople) { //сетер для поля  people
        this.pepople = pepople;
    }

    public int getFloor() { //гетер для поля floor
        return floor;
    }

    public void setFloor(int floor) { //сетер для поля floor
        this.floor = floor;
    }

    public float getArea() { // гетер для поля area
        return area;
    }

    public void setArea(float area) { // сетер для поля area
        this.area = area;
    }

    public String getCity() {// гетер для поля city
        return city;
    }

    public void setCity(String city){ // сетер для поля city
        this.city = city;
    }

    @Override // переопределение метода toString()
    public String toString() {
        return "Apartament{" +
                "rooms=" + rooms +
                ", pepople=" + pepople +
                ", floor=" + floor +
                ", area=" + area +
                ", city='" + city + '\'' +
                '}';
    }
}
