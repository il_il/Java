package ua.org.oa.il_il.lb3.task2;

public class Planes {
    private int carryingCapacity;
    private String name;

    public Planes (){};
    public Planes(int carryingCapacity, String name) {
        this.carryingCapacity = carryingCapacity;
        this.name = name;
    }
    public Planes(Planes planes){
        carryingCapacity = planes.getCarryingCapacity();
        name = planes.getName();
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Planes{" +
                "carryingCapacity=" + carryingCapacity +
                ", name='" + name + '\'' +
                '}';
    }
}
