package ua.org.oa.il_il.pz1;

public class ComparisonOperators {
    private int number;

    ComparisonOperators(){};
    ComparisonOperators(int number){
        this.number = number;
    }
    ComparisonOperators(ComparisonOperators comparisonOperators){
        number = comparisonOperators.getNumber();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void scanNumber(){
        if(number == 0){
            System.out.println("* – «нулевое число»;");
        }
        else if(number>0){
            if(number%2 == 0){
                System.out.println("* – «положительное четное число»;");
            }
            else{
                System.out.println("* – «положительное нечетное число»;");
            }
        }
        else{
            if((number*(-1))%2 == 0){
                System.out.println("* – «отрицательное четное число»;");
            }
            else{
                System.out.println("* – «положительное нечетное число»;");
            }
        }
    }
}
